# Material
- A USB key of 4 Giga for the iso file
- A USB or hard drive of minimum 25 Giga.[^1]

# Create the iso

* You can try [this tutorial](https://www.tecmint.com/install-linux-os-on-usb-drive/) if you are already on linux or Mac. For windows you can try [this](https://www.howtogeek.com/howto/linux/create-a-bootable-ubuntu-usb-flash-drive-the-easy-way/). do not hesitate to search for other methods if these do not work for you.
* You will need to know where your usb has been mounted, personally I used [mount](https://superuser.com/questions/361885/how-do-i-figure-out-which-dev-is-a-usb-flash-drive) or the Disk Utility software on a mac. lsblk  on linux.

* The most straightforward, if you are familiar with terminal, is to use a [cp](https://superuser.com/questions/620877/cp-command-to-make-bootable-iso-image-usb) or a [dd](https://askubuntu.com/questions/372607/how-to-create-a-bootable-ubuntu-usb-flash-drive-from-terminal) command [^2]


# Figure out how to boot from USB
* This is specific to your computer so you need to browse the internet. Search in your favorit search engine for the following: "how to access bios of a [name of device model here]"
For instance for my dell I wrote "how to access bios of a dell latitude 7420" to figure out I just have to press F2 during boot.
And if you are on mac, search how to access efi of a [name of your device here]
* Insert your USB key with the iso on it.
* Once in the bios search for some menu about boot priority. In mine it was called "Boot Configuration" figure out what is name your USB device and switch it to the top and/or switch your windows system to the bottom.

#  Disable internal hard drive (skip if you will not install)
## Solution 1: Bios options
* This may vary with your computer too. But look for a menu called "storage" and disable the integrated storage device controller. You can also look for keywords such as "NVMe" "HDD" "hard drive"
* You may have to disable the UEFI "secure boot" too
* I never tried to do it on MAC so you will have to browse internet on this or try solution 2
## Solution 2: Unscrew your computer
That might be scary, but it is generally well documented for beginners. Hard drives are removable. If you get the right screw it is very easy to remove it.
* This is specific to your computer so you need to browse the internet. Search something like "remove internal hard drive from [computer model]"
* This is also valid for MAC pros!
* Warning! always make sure to remove the battery (generally instructed in the tutorials)
* Although rare, electrostatic damage may arise, to avoid this, make sure to be connected to a radiator or any thing metaling connected to ground.

# (optional) Load it to RAM
You just have one usb? load it to RAM!
https://askubuntu.com/questions/829917/can-i-boot-a-live-usb-fully-to-ram-allowing-me-to-remove-the-disk

# booting
You are ready! 
* skip the harddisk check by pressing ctrl+c

## Images of non-free linux debians I downloaded to make the live usb:
https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/

https://www.debian.org/releases/bullseye/installmanual


[1]: There is ways so get by with even smaller USB if you choose distributions like xubuntu, lubuntu, puppy linux...
[2]: As an example on my mac computer the command I had to do was:
 `sudo cp /Users/marie/Download/pop-os_21.04_amd64_intel_10.iso /dev/disk3 `